import Foundation

class Session {
    private var session: URLSession
    private let semaphore = DispatchSemaphore(value: 0)
    
    init() {
        self.session = URLSession.shared
    }
    
    private func makeParametersString(_ parameters: [[String: String]]) -> String {
        var encodedParameters = ""
        for parameter in parameters {
            if !parameter["parameter"]!.isEmpty || !parameter["value"]!.isEmpty {
                encodedParameters += parameter["parameter"]! + "=" + URLEncode(parameter["value"]!) + "&"
            }
        }
        if !encodedParameters.isEmpty {
            encodedParameters.removeLast()
        }
        return encodedParameters
    }
    
    public func makeGET(url: String, parameters: [[String: String]], userAgent: String) -> (data: Data?, response: URLResponse?, error: Error?) {
        var data: Data?
        var response: URLResponse?
        var error: Error?
        
        var encodedParameters = makeParametersString(parameters)
        if !encodedParameters.isEmpty {
            encodedParameters.insert("?", at: encodedParameters.startIndex)
        }
        guard let url = URL(string: url + encodedParameters) else { return (nil, nil, nil) }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        
        session.dataTask(with: request) { (dt, res, err) in
            data = dt
            response = res
            error = err
            self.semaphore.signal()
            }.resume()
        self.semaphore.wait()
        return (data, response, error)
    }
    
    public func makePOST(url: String, parameters: [[String: String]], userAgent: String) -> (data: Data?, response: URLResponse?, error: Error?) {
        var data: Data?
        var response: URLResponse?
        var error: Error?
        
        let encodedParameters = makeParametersString(parameters)
        guard let url = URL(string: url + "?" + encodedParameters) else { return (nil, nil, nil) }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        request.httpBody = encodedParameters.data(using: .utf8)
        
        session.dataTask(with: request) { (dt, res, err) in
            data = dt
            response = res
            error = err
            self.semaphore.signal()
            }.resume()
        self.semaphore.wait()
        
        return (data, response, error)
    }
    
    private func URLEncode(_ str: String) -> String {
        let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] |").inverted)
        return str.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) ?? ""
    }
    
    private func URLDecode(_ str: String) -> String {
        let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] |").inverted)
        return str.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) ?? ""
    }
}
