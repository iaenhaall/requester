import Foundation
import Cocoa

class PlaceholderTextView: NSTextView {
    @objc var placeholderAttributedString: NSAttributedString?
}
