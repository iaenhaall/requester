import Cocoa

@IBDesignable
class Button: NSButton {
    @IBInspectable var textColor: NSColor?
    
    override func awakeFromNib()
    {
        if let textColor = textColor, let font = font
        {
            let style = NSMutableParagraphStyle()
            style.alignment = .center
            
            let attributes =
                [
                    NSAttributedStringKey.foregroundColor: textColor,
                    NSAttributedStringKey.font: font,
                    NSAttributedStringKey.paragraphStyle: style
                    ] as [NSAttributedStringKey : Any]
            
            let attributedTitle = NSAttributedString(string: title, attributes: attributes)
            self.attributedTitle = attributedTitle
        }
    }
    
    override func draw(_ dirtyRect: NSRect)
    {
        super.draw(dirtyRect)
    }
}
