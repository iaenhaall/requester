import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSTableViewDataSource, NSTableViewDelegate {
    
    @IBOutlet weak var window: NSWindow!
    @IBOutlet weak var URLTextField: NSTextField!
    @IBOutlet weak var userAgentTextField: NSTextField!
    @IBOutlet weak var responseTextView: PlaceholderTextView!
    @IBOutlet weak var dataTextView: PlaceholderTextView!
    @IBOutlet weak var progressIndicator: NSProgressIndicator!
    @IBOutlet weak var notificationLable: NSTextField!
    @IBOutlet weak var HTTPVerbComboBox: NSComboBox!
    @IBOutlet weak var parametersTableView: NSTableView!
    @IBOutlet weak var responseLable: NSTextField!
    @IBOutlet weak var parametersCheckBox: NSButton!
    
    private var parameters: [[String: String]] = [["parameter": "", "value": ""]]
    private var dragDropType = NSPasteboard.PasteboardType(rawValue: "private.table-row")
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        notificationLable.stringValue = ""
        responseTextView.string = ""
        dataTextView.string = ""
        responseTextView.placeholderAttributedString = NSAttributedString(string: "Execute your request to display the response", attributes: [NSAttributedStringKey.foregroundColor : NSColor.lightGray])
        dataTextView.placeholderAttributedString = NSAttributedString(string: "Execute your request to display the data", attributes: [NSAttributedStringKey.foregroundColor : NSColor.lightGray])
        userAgentTextField.nextKeyView = parametersTableView
        parametersTableView.nextKeyView = URLTextField
        parametersTableView.dataSource = self
        parametersTableView.delegate = self
        parametersTableView.registerForDraggedTypes([dragDropType])
    }
    
    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }

    //MARK: TableViwe
    // Returns the number of rows in a table
    func numberOfRows(in tableView: NSTableView) -> Int {
        return parameters.count
    }

    // Sets cell values
    func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
        return parameters[row][(tableColumn?.identifier.rawValue)!]
    }
    
    // Updates parameter values when changing
    func tableView(_ tableView: NSTableView, setObjectValue object: Any?, for tableColumn: NSTableColumn?, row: Int) {
        parameters[row][(tableColumn?.identifier.rawValue)!] = object as? String ?? ""
    }
    
    // Moving rows
    // Enable multiple item dragging
    func tableView(_ tableView: NSTableView, pasteboardWriterForRow row: Int) -> NSPasteboardWriting? {
        let item = NSPasteboardItem()
        item.setString(String(row), forType: self.dragDropType)
        return item
    }

    // Determines a valid drop target
    func tableView(_ tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableView.DropOperation) -> NSDragOperation {
        if dropOperation == .above {
            return .move
        }
        return []
    }
    
    // Moves rows and update table
    func tableView(_ tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableView.DropOperation) -> Bool {
        var oldIndexes = [Int]()
        info.enumerateDraggingItems(options: [], for: tableView, classes: [NSPasteboardItem.self], searchOptions: [:]) { dragItem, _, _ in
            if let str = (dragItem.item as! NSPasteboardItem).string(forType: self.dragDropType), let index = Int(str) {
                oldIndexes.append(index)
            }
        }
        
        var oldIndexOffset = 0
        var newIndexOffset = 0
        
        for oldIndex in oldIndexes {
            if oldIndex < row {
                let rowValue = parameters[oldIndex + oldIndexOffset]
                parameters[oldIndex + oldIndexOffset] = parameters[row - 1]
                parameters[row - 1] = rowValue
                oldIndexOffset -= 1
            } else {
                let rowValue = parameters[oldIndex]
                parameters[oldIndex] = parameters[row + newIndexOffset]
                parameters[row + newIndexOffset] = rowValue
                newIndexOffset += 1
            }
        }
        tableView.reloadData()
        
        return true
    }
    
    // MARK: Secondary functions
    private func copyToPasteBoard(_ string: String) {
        let pasteboard = NSPasteboard.general
        pasteboard.declareTypes([NSPasteboard.PasteboardType.string], owner: nil)
        pasteboard.setString(string, forType: NSPasteboard.PasteboardType.string)
    }
    
    private func setDefaultValuesForResponseLable() {
        if responseLable.stringValue != "Response:" {
            responseLable.stringValue = "Response:"
            responseLable.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    //MARK: Button implementation
    // Add and Remove parameters buttons
    @IBAction func parametersSegmentControl(_ sender: NSSegmentedControl) {
        parametersTableView.beginUpdates()
        switch sender.selectedSegment
        {
        case 0:
            if parameters.count == 0 {
                sender.setEnabled(true, forSegment: 1)
            }
            parametersTableView.insertRows(at: IndexSet(integer: parameters.count), withAnimation: .slideDown)
            parameters.append(["parameter": "", "value": ""])
        case 1:
            if parameters.count > 0 {
                let selectedRow = parametersTableView.selectedRow
                if selectedRow >= 0 {
                    parametersTableView.removeRows(at: IndexSet(integer: parametersTableView.selectedRow), withAnimation: .slideUp)
                    parameters.remove(at: selectedRow)
                } else {
                    parametersTableView.removeRows(at: IndexSet(integer: parameters.count - 1), withAnimation: .slideUp)
                    parameters.removeLast()
                }
            }
            if parameters.count == 0 {
                sender.setEnabled(false, forSegment: 1)
            }
        default:
            break
        }
        parametersTableView.endUpdates()
    }
    
    // Clear buttons
    @IBAction func clearResponseButton(_ sender: NSButton) {
        responseTextView.string = ""
        setDefaultValuesForResponseLable()
    }
    
    @IBAction func clearDataButton(_ sender: NSButton) {
        dataTextView.string = ""
    }
    
    @IBAction func clearParametersButton(_ sender: NSButton) {
        parameters.removeAll()
        parameters.append(["parameter": "", "value": ""])
        parametersTableView.reloadData()
    }
    
    @IBAction func clearAllButton(_ sender: NSButton) {
        notificationLable.stringValue = ""
        URLTextField.stringValue = ""
        userAgentTextField.stringValue = ""
        clearResponseButton(sender)
        clearDataButton(sender)
        clearParametersButton(sender)
        setDefaultValuesForResponseLable()
    }

    // Copy buttons
    @IBAction func copyResponseButton(_ sender: NSButton) {
        copyToPasteBoard(responseTextView.string)
    }
    
    @IBAction func copyDataButton(_ sender: NSButton) {
        copyToPasteBoard(dataTextView.string)
    }
    
    // Execute button
    @IBAction func executeButton(_ sender: NSButton) {
        notificationLable.stringValue = ""
        setDefaultValuesForResponseLable()
        parametersTableView.abortEditing()
        if HTTPVerbComboBox.stringValue.isEmpty {
            notificationLable.stringValue = "Choose HTTP verb"
            return
        }
        if URLTextField.stringValue.isEmpty {
            notificationLable.stringValue = "Enter URL address"
            return
        }
        
        let url = URLTextField.stringValue
        let HTTPVerb = HTTPVerbComboBox.stringValue
        let userAgent = userAgentTextField.stringValue
        let queue = DispatchQueue.global(qos: .userInitiated)
        var parameters: [[String: String]] = []
        if parametersCheckBox.state == .on {
            parameters = self.parameters
        }
        
        queue.async{
            DispatchQueue.main.async {
                self.progressIndicator.startAnimation(self)
            }
            
            let session = Session()
            var result: (data: Data?, response: URLResponse?, error: Error?)
            switch HTTPVerb {
                case "GET":
                    result = session.makeGET(url: url, parameters: parameters, userAgent: userAgent)
                case "POST":
                    result = session.makePOST(url: url, parameters: parameters, userAgent: userAgent)
                default:
                    break
            }
            
            DispatchQueue.main.async {
                if let error = result.error {
                    self.notificationLable.stringValue = "An error occurred while executing the request"
                    self.responseLable.textColor = #colorLiteral(red: 0.8, green: 0, blue: 0, alpha: 1) // 255 86 79 || 204 0 0
                    self.responseLable.stringValue = "Error:"
                    self.responseTextView.string = error.localizedDescription
                    self.dataTextView.string = ""
                } else {
                    if let response = result.response, let data = result.data {
                        self.responseTextView.string = response.description
                        self.dataTextView.string = String(data: data, encoding: String.Encoding.utf8) ?? "No text data"
                    } else {
                        self.notificationLable.stringValue = "Incorrect URL value"
                    }
                }
                self.progressIndicator.stopAnimation(self)
            }
        }
    }
    
}
